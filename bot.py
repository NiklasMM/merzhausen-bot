import argparse
import datetime
import logging
import pathlib
import re

import requests
from bs4 import BeautifulSoup
from mastodon import Mastodon

logging.basicConfig(
    filename=pathlib.Path(__file__).parent.resolve() / "mylog.log", level="INFO"
)


def parse_entry(entry, posted_urls):
    """
    Parses an article into a nice dict
    """
    data = {
        "url": "https://www.merzhausen.de" + entry.find("a").get("href"),
        "title": entry.find("h3").get_text("\n").strip(),
        "_entry": entry,
    }

    # if the url has already been posted, just set the date to 100 days in the
    # past, it will be sorted out later anyway, so we can avoid getting the page
    if data["url"] in posted_urls:
        data["date"] = datetime.date.today() - datetime.timedelta(days=100)
    else:
        # get date
        response = requests.get(data["url"])
        article = BeautifulSoup(response.text, features="html.parser")

        date = (
            article.find(class_="hw_record__creation_date")
            .find_all("span")[1]
            .getText()
        )
        data["date"] = datetime.datetime.strptime(date, "%d.%m.%Y").date()

    return data


def toot(entry, mastodon, dry_run):
    """
    Takes an article and posts it on Mastodon
    """
    toot_content = f"Neuer Eintrag auf Merzhausen.de vom {entry['date'].strftime('%d.%m.%Y')}:\n\n{entry['title']}\n\n{entry['url']}"
    if dry_run:
        print(toot_content)
    else:
        mastodon.status_post(toot_content)


def get_posted_urls(mastodon) -> set[str]:
    """
    Returns a set of urls to all articles that have already been posted.
    """
    toots = mastodon.account_statuses(mastodon.me()["id"])
    urls = set()
    for toot in toots:
        if m := re.search(r"https://\S*", toot["content"]):
            urls.add(m.group().strip('"'))
    return urls


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Toots about official notices on the Merzhausen website"
    )
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="If given only prints the content of the toot",
    )
    parser.add_argument(
        "access_token", type=str, help="access token for the targeted Mastodon account."
    )

    args = parser.parse_args()

    urls = [
        "https://www.merzhausen.de/rathaus-service/aktuelles/neuigkeiten",
        "https://www.merzhausen.de/rathaus-service/aktuelles/oeffentliche-bekanntmachungen/aktuelle-oeffentliche-bekanntmachungen",
        "https://www.merzhausen.de/rathaus-service/aktuelles/oeffentliche-bekanntmachungen/archiv-oeffentliche-bekanntmachungen",
    ]

    entries = []

    mastodon = Mastodon(
        api_base_url="https://freiburg.social", access_token=args.access_token
    )

    posted_urls = get_posted_urls(mastodon)

    for url in urls:
        response = requests.get(url)

        soup = BeautifulSoup(response.text, features="html.parser")
        page_entries = [
            parse_entry(e, posted_urls=posted_urls)
            for e in soup.find_all(class_="hwnews__record")
        ]
        logging.info(f"Found {len(page_entries)} entries on url {url}")
        entries.extend(page_entries)

    logging.info(f"Found {len(entries)} entries.")

    # filter out entries older than a month
    entries = [
        e
        for e in entries
        if (e["date"] > datetime.date.today() - datetime.timedelta(weeks=4))
    ]

    for entry in sorted(entries, key=lambda x: x["date"]):
        if entry["url"] in posted_urls:
            logging.info(f"Already posted '{entry['title']}")
        else:
            logging.info(f"Will now post new article '{entry['title']}'")
            toot(entry, mastodon, dry_run=args.dry_run)
